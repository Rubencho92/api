<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route:: get('restaurantes', 'App\Http\Controllers\RestaurantesController@getRestaurante'); // metodo get para consultar todo en la BD

Route:: get('restaurantes/{id}','App\Http\Controllers\RestaurantesController@getRestaurantexid'); // metodo get para consultar un dato especifico en la BD

Route:: post('addRestaurantes','App\Http\Controllers\RestaurantesController@insertRestaurante'); // metodo post insertar datos en la BD

Route::put('updateRestaurantes/{id}','App\Http\Controllers\RestaurantesController@updateRestaurante'); // metodo put para editar dato en la BD

Route::delete('deleteRestaurantes/{id}','App\Http\Controllers\RestaurantesController@deleteRestaurante'); // metodo delete para eliminar dato de la BD 

Route::get('statistics/{lat}/{lng}/{radio}','App\Http\Controllers\RestaurantesController@index');