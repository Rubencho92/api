<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurantes;
use Illuminate\Support\Facades\DB;


class RestaurantesController extends Controller
{
    public function getRestaurante(){ // consulta todos los resgistros de la base
        return response()-> json(Restaurantes::all(), 200);
    }

    public function getRestaurantexid($id){ // consulta un dato especifico de la base
        $restaurante = Restaurantes::find($id);
        if(is_null($restaurante)){
            return response()->json(['Mensaje'=>'Registro no encontrado'],400);
        }
        return response()-> json($restaurante::find($id),200);
    }

    public function index($lat,$lng,$radio){
        $restaurante = DB::table('restaurantes') 
                                            ->select(DB::raw('count(*),AVG(rating),std(rating),( 6371 * ACOS( 
                                                COS( RADIANS(' . $lat . ') ) 
                                                * COS(RADIANS( lat ) ) 
                                                * COS(RADIANS( lng ) 
                                                - RADIANS(' . $lng . ') ) 
                                                + SIN( RADIANS(' . $lat . ') ) 
                                                * SIN(RADIANS( lat ) ) 
                                               )) as distance'))
                                            ->whereBetween('lat',[$lat,$lng])
                                            ->whereBetween('lng',[$lat,$lng])
                                            ->having(DB::raw('distance <'.$radio.''))
                                            ->groupBy('distance')
                                            ->get();
    
                                            return $restaurante;
    }

    public function insertRestaurante(Request $request){ // insertar datos en la base D.
        $restaurante = Restaurantes::create($request -> all());
        return response($restaurante,200);
    }

    public function updateRestaurante(Request $request, $id){ // actualizar datos en la base D.
        $restaurante = Restaurantes::find($id);
        if(is_null($restaurante)){
            return response()->json(['Mensaje'=>'Registro no encontrado'],400);
        }
        $restaurante -> update($request -> all());
        return response($restaurante,200);
    }

    public function deleteRestaurante($id){ // eliminar dato en la base
        $restaurante = Restaurantes::find($id);
        if(is_null($restaurante)){
            return response()->json(['Mensaje'=>'Registro no encontrado'],400);
        }
        $restaurante->delete();
        return response()->json(['Mensaje' => 'Registro Eliminado'],200);

    }


}


