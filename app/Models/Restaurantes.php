<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurantes extends Model
{
 //   use HasFactory;
 public $timestamps = false; // para que no traiga el tiempo que se inserto los datos a la DB
 protected $fillable = ['id', 'rating', 'name', 'site','email','phone','street','city','state','lat','lng']; // para traer los campos que necesitamos de la DB
 
}
